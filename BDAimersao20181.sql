
SELECT * FROM cliente;

SELECT nome, codigo, tipo FROM cliente;

SELECT DISTINCT tipo FROM cliente;

SELECT * FROM funcionario;

SELECT nome, salario FROM funcionario
WHERE salario = 300;

SELECT nome, salario FROM funcionario
WHERE salario BETWEEN 200 AND 300;

SELECT * FROM funcionario
WHERE nome LIKE '%Silva%' OR nome LIKE '%Tavares';

SELECT nome, setor FROM funcionario
WHERE setor LIKE UPPER('cov');

SELECT nome, setor, email FROM funcionario
WHERE email IS NULL;

SELECT nome, setor, naturalidade FROM funcionario
WHERE naturalidade IN (5,6);

SELECT nome, ABS(codigo) FROM cliente; --transforma numeros negativos em positivo.

SELECT nome, salario, MOD(salario,150) FROM funcionario --resto da divisão do salario por 150
WHERE salario BETWEEN 200 AND 300;

SELECT nome FROM funcionario -- Exibir em ordem crescente ou decrestente, ASC e DESC
ORDER BY nome DESC;

UPDATE funcionario SET salario = 666
WHERE salario = 290;

SELECT * FROM funcionario;

UPDATE funcionario SET salario = 1 --buga não sei pq.
WHERE salario IN (SELECT salario FROM funcionario WHERE setor LIKE 'COV');

CREATE TABLE teste(
	codigo int
);
INSERT INTO teste VALUES(1);
INSERT INTO teste VALUES(2);
SELECT * FROM teste;
DROP TABLE teste;
DELETE FROM teste;
--WHERE codigo =2;

SELECT sexo, COUNT(*) AS quantidade FROM funcionario
GROUP BY sexo
HAVING COUNT(*) > 2;


SELECT * FROM cidade;

SELECT nome, cidade FROM cliente;

SELECT C.nome AS nome_cliente, CI.nome --Usar o código da cidade na tabela cliente que é uma chave estrangeiro pois é o 
FROM cliente C INNER JOIN cidade CI    --codigo da cidade ba tabela cidade.
ON(C.cidade = CI.codigo);

SELECT * FROM produto;
SELECT * FROM tipo;

SELECT P.nome, T.nome AS tipo
FROM produto P INNER JOIN tipo T
ON(p.tipo = t.codigo)

SELECT * FROM pedido;
SELECT * FROM itens;
SELECT * FROM produto;

SELECT PR.nome
FROM itens I
INNER JOIN pedido P on (I.pedido = P.codigo)
INNER JOIN produto PR on (PR.codigo = I.produto)
WHERE P.datapedido BETWEEN '2007-01-01' AND '2007-01-31'; 
